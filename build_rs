use std::env;
use std::fs::File;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::rc::Rc;
use deno_core::{Extension, extension, JsRuntimeForSnapshot, ModuleCodeString, ModuleName, op2, OpState, RuntimeOptions, SourceMapData, v8};
use deno_core::error::AnyError;
use deno_ast::{EmittedSourceBytes, MediaType, TranspileOptions};
use deno_ast::ParseParams;
use deno_ast::SourceMapOption;
use deno_ast::SourceTextInfo;
use deno_core::url::Url;
use deno_core::anyhow::Error;

#[op2]
fn op_use_state(
    state: &mut OpState,
    #[global] callback: v8::Global<v8::Function>,
) -> Result<(), Error> {
    state.put(callback);
    Ok(())
}

fn main() {
    let o = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    let snapshot_path = o.join("runtime.bin");

    let extensions_for_snapshot = vec![runtime_base::init_ops()];
    // Create the snapshot.
    let snapshot = deno_core::snapshot::create_snapshot(deno_core::snapshot::CreateSnapshotOptions {
        cargo_manifest_dir: env!("CARGO_MANIFEST_DIR"),
        startup_snapshot: None,
        skip_op_registration: false,
        extensions: extensions_for_snapshot,
        extension_transpiler: Some(Rc::new(|specifier, source| {
            maybe_transpile_source(specifier, source)
        })),
        with_runtime_cb: None,
    }, None).expect("Failed to create snapshot");

    for file in snapshot.files_loaded_during_snapshot {
        println!("cargo:rerun-if-changed={}", file.to_str().unwrap());
    }

    let mut output = File::create(snapshot_path).expect("Failed to create snapshot file");
    output.write(&snapshot.output).expect("Failed to write snapshot");
}

// modified from https://github.com/denoland/deno_core/raw/main/testing/checkin/runner/ts_module_loader.rs
pub fn maybe_transpile_source(
    specifier: ModuleName,
    source: ModuleCodeString,
) -> Result<(ModuleCodeString, Option<SourceMapData>), AnyError> {
    let media_type = MediaType::from_path(Path::new(&specifier));

    match media_type {
        MediaType::TypeScript => {}
        MediaType::JavaScript => return Ok((source, None)),
        MediaType::Mjs => return Ok((source, None)),
        _ => panic!(
            "Unsupported media type for snapshotting {media_type:?} for file {}",
            specifier
        ),
    }

    let parsed = deno_ast::parse_module(ParseParams {
        specifier: Url::parse(&specifier).unwrap(),
        media_type,
        capture_tokens: false,
        scope_analysis: false,
        maybe_syntax: None,
        text: source.into(),
    })?;
    let transpiled_source = parsed.transpile(&TranspileOptions::default(), &deno_ast::EmitOptions {
        source_map: SourceMapOption::Separate,
        inline_sources: false,
        ..Default::default()
    })?.into_source();

    match transpiled_source {
        EmittedSourceBytes { source, source_map } => {
            Ok((String::from_utf8(source)?.into(), source_map.map(|s| s.into())))
        }
    }
}