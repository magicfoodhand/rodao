# rodao_runtime

Yet another JavaScript runtime, heavily built on the work done by the [Deno](https://deno.land) team.

## Why?
After using Cloudflare Workers and JVM embedded scripting languages for a few years, I wondered what it would take 
to build my own version of that; of course the final result had to be in Rust though, or else it wouldn't count. 

Unfortunately that's a ton of work, luckily Deno built most of what I need though, so I'll start from there.

