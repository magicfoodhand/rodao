use crate::ts_module_loader::{SourceMapStore, TypescriptModuleLoader};
use anyhow::{anyhow, Result};
use deno_ast::{EmittedSourceBytes, MediaType, ParseParams, SourceMapOption, TranspileOptions};
use deno_core::error::AnyError;
use deno_core::snapshot::CreateSnapshotOutput;
use deno_core::url::Url;
use deno_core::v8::{Global, Object};
use deno_core::{
    extension, serde_v8, v8, Extension, JsRuntime, ModuleCodeString, ModuleName, ModuleSpecifier,
    RuntimeOptions, SourceMapData,
};
use deno_fetch;
use deno_fetch::FsFetchHandler;
use deno_permissions::PermissionsContainer;
use serde_json::Value;
use std::cell::RefCell;
use std::collections::HashMap;
use std::path::Path;
use std::rc::Rc;

mod ts_module_loader;

pub struct Request {
    pub method: String,
    pub path: String,
    pub headers: Vec<(String, String)>,
    pub body: Vec<u8>,
}

fn source_map_store() -> SourceMapStore {
    SourceMapStore(Rc::new(RefCell::new(HashMap::new())))
}

extension!(
    rodao_runtime,
    deps = [
        deno_webidl,
        deno_console,
        deno_url,
        deno_web,
        deno_fetch,
        deno_net,
        deno_tls,
        deno_websocket,
        deno_crypto
    ],
    esm_entry_point = "ext:runtime/std_lib/index.js",
    esm = ["std_lib/index.js"]
);

pub fn base_extensions() -> Vec<Extension> {
    vec![
        deno_webidl::deno_webidl::init_ops_and_esm(),
        deno_console::deno_console::init_ops_and_esm(),
        deno_url::deno_url::init_ops_and_esm(),
        deno_web::deno_web::init_ops_and_esm::<PermissionsContainer>(Default::default(), None),
        deno_crypto::deno_crypto::init_ops_and_esm(Default::default()),
        deno_fetch::deno_fetch::init_ops_and_esm::<PermissionsContainer>(deno_fetch::Options {
            file_fetch_handler: Rc::new(FsFetchHandler),
            ..Default::default()
        }),
        deno_net::deno_net::init_ops_and_esm::<PermissionsContainer>(None, None),
        deno_tls::deno_tls::init_ops_and_esm(),
        deno_websocket::deno_websocket::init_ops_and_esm::<PermissionsContainer>(
            Default::default(),
            Default::default(),
            Default::default(),
        ),
        rodao_runtime::init_ops_and_esm(),
    ]
}

fn create_runtime(store: Option<SourceMapStore>) -> JsRuntime {
    let extensions = base_extensions();
    let store = store.unwrap_or_else(|| source_map_store());
    JsRuntime::new(RuntimeOptions {
        extensions,
        module_loader: Some(Rc::new(TypescriptModuleLoader {
            source_maps: store.clone(),
        })),
        source_map_getter: Some(Rc::new(store)),
        ..Default::default()
    })
}

pub async fn run_js(contents: String) -> Result<Value, AnyError> {
    let mut context = create_runtime(None);
    let output: Value = eval(&mut context, contents).await.expect("Eval failed");
    Ok(output)
}

pub async fn snapshot(
    context: &mut JsRuntime,
    code: String,
    extensions: Vec<Extension>,
) -> Result<CreateSnapshotOutput> {
    let specifier = ModuleSpecifier::parse("file://input.js")?;
    let mod_id = context
        .load_main_es_module_from_code(&specifier, code)
        .await?;

    let f = context.mod_evaluate(mod_id);
    context.run_event_loop(Default::default()).await?;
    let res = f.await;

    if res.is_err() {
        return Err(anyhow!("Failed to Evaluate Module: {:?}", res.err()));
    }

    let snapshot = deno_core::snapshot::create_snapshot(
        deno_core::snapshot::CreateSnapshotOptions {
            cargo_manifest_dir: env!("CARGO_MANIFEST_DIR"),
            startup_snapshot: None,
            skip_op_registration: false,
            extensions,
            extension_transpiler: Some(Rc::new(|specifier, source| {
                maybe_transpile_source(specifier, source)
            })),
            with_runtime_cb: None,
        },
        None,
    )
    .expect("Failed to create snapshot");
    return Ok(snapshot);
}

// modified from - https://github.com/denoland/deno_core/raw/main/core/examples/eval_js_value.rs
async fn eval(context: &mut JsRuntime, code: String) -> Result<Value, AnyError> {
    let module = load_module(code, context).await?;

    let scope = &mut context.handle_scope();
    let result = {
        let module_obj = module.open(scope);
        let export_identifier = v8::String::new(scope, "default").unwrap();

        let default_export = module_obj.get(scope, export_identifier.into()).unwrap();
        let default_obj = v8::Local::<Object>::try_from(default_export).unwrap();

        let identifier = v8::String::new(scope, "fetch").unwrap();
        let function_value = default_obj.get(scope, identifier.into()).unwrap();
        if function_value.is_function() {
            let function = v8::Local::<v8::Function>::try_from(function_value).unwrap();
            match function.call(scope, default_export, &[]) {
                None => return Err(anyhow!("Something went wrong during execution")),
                Some(v) => v,
            }
        } else {
            return Err(anyhow!("No fetch function"));
        }
    };

    if result.is_promise() {
        let promise = v8::Local::<v8::Promise>::try_from(result).unwrap();
        let promise_result = promise.result(scope);
        let deserialized_value = serde_v8::from_v8::<Value>(scope, promise_result);
        return match deserialized_value {
            Ok(value) => Ok(value),
            Err(err) => Err(anyhow!("Cannot deserialize value: {err:?}")),
        };
    }

    let deserialized_value = serde_v8::from_v8::<Value>(scope, result);
    match deserialized_value {
        Ok(value) => Ok(value),
        Err(err) => Err(anyhow!("Cannot deserialize value: {err:?}")),
    }
}

async fn load_module(code: String, context: &mut JsRuntime) -> Result<Global<Object>> {
    let specifier = ModuleSpecifier::parse("file://input.js")?;
    let mod_id = context
        .load_main_es_module_from_code(&specifier, code)
        .await?;

    let f = context.mod_evaluate(mod_id);
    context.run_event_loop(Default::default()).await?;
    let res = f.await;

    if res.is_err() {
        return Err(anyhow!("Failed to Evaluate Module: {:?}", res.err()));
    }

    context.get_module_namespace(mod_id)
}

// modified from https://github.com/denoland/deno_core/raw/main/testing/checkin/runner/ts_module_loader.rs
fn maybe_transpile_source(
    specifier: ModuleName,
    source: ModuleCodeString,
) -> Result<(ModuleCodeString, Option<SourceMapData>), AnyError> {
    let media_type = MediaType::from_path(Path::new(&specifier));

    match media_type {
        MediaType::TypeScript => {}
        MediaType::JavaScript => return Ok((source, None)),
        MediaType::Mjs => return Ok((source, None)),
        _ => panic!(
            "Unsupported media type for snapshotting {media_type:?} for file {}",
            specifier
        ),
    }

    let parsed = deno_ast::parse_module(ParseParams {
        specifier: Url::parse(&specifier).unwrap(),
        media_type,
        capture_tokens: false,
        scope_analysis: false,
        maybe_syntax: None,
        text: source.into(),
    })?;
    let transpiled_source = parsed
        .transpile(
            &TranspileOptions::default(),
            &deno_ast::EmitOptions {
                source_map: SourceMapOption::Separate,
                inline_sources: false,
                ..Default::default()
            },
        )?
        .into_source();

    match transpiled_source {
        EmittedSourceBytes { source, source_map } => Ok((
            String::from_utf8(source)?.into(),
            source_map.map(|s| s.into()),
        )),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let runtime = tokio::runtime::Builder::new_current_thread()
            .enable_all()
            .build()
            .unwrap();
        let result = runtime.block_on(run_js(
            r#"
            export default {
              fetch(request, env, ctx) {
                console.error({ request, env, ctx });
                return fetch
              },
            };
        "#
            .to_string(),
        ));
        if let Err(error) = result {
            panic!("run failed: {}", error);
        }
        println!("{:?}", &result.unwrap());
        assert_eq!(1, 1);
    }
}
