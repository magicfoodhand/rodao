use anyhow::Result;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use std::process::exit;
use clap::{CommandFactory, Parser};
use clap_derive::{Args, Subcommand};
use serde::Deserialize;
use crate::logger::setup_logger;

mod logger;

#[derive(Parser)]
#[command(version, about, long_about = None)]
#[command(next_line_help = true)]
pub struct CLI {
    #[arg(short, long, value_name = "FILE", env = "RODAO_CONFIG")]
    config: Option<PathBuf>,

    #[arg(short, long, env = "RODAO_VERBOSE", default_value = "0")]
    verbose: i8,

    #[arg(short, long, env = "RODAO_OUTPUT")]
    output: Option<String>,

    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand, Debug)]
pub enum Commands {
    #[command(about = "Run the runtime")]
    Run(RunArgs),
}

#[derive(Args, Debug, Default)]
pub struct RunArgs {
    #[arg(short, long, action)]
    enabled: bool,
}

#[derive(Deserialize, Debug)]
struct Options {

}

impl Options {
    fn default() -> Self {
        Options {}
    }
}

impl CLI {
    fn options(&self) -> Result<Options> {
        let options = match &self.config {
            None => Options::default(),
            Some(config_path) => {
                let mut file = File::open(config_path)?;
                let mut contents = String::new();
                file.read_to_string(&mut contents)?;
                serde_json::from_str(contents.as_str())?
            }
        };
        Ok(options)
    }
}


fn main() {
    let cli = CLI::parse();
    if cli.command.is_none() {
        let mut app = CLI::command();
        app.print_help().expect("`print_help` failed");
        exit(0);
    }

    setup_logger(&cli);
    let options = cli.options().expect("Failed to parse options");

    match cli.command.unwrap() {
        Commands::Run(_) => {
            println!("options: {:?}", options);
        }
    }
}
